package com.example.practica03java;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CalculadoraActivity extends AppCompatActivity {
    private Button btnSumar;
    private Button btnRestar;
    private Button btnMulti;
    private Button btnDiv;
    private Button btnLimpiar;
    private Button btnRegresar;
    private TextView lblUsuario;
    private TextView lblResultado;
    private EditText txtUno;
    private EditText txtDos;

    // Declarar el objeto Calculadora
    private Calculadora calculadora = new Calculadora(0,0);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        iniciarComponentes();
        // Obtener los datos del MainActivity
        Bundle datos = getIntent().getExtras();
        String usuario = datos.getString("usuario");
        lblUsuario.setText(usuario);

        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSumar();
            }
        });

        btnRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnRestar();
            }
        });

        btnMulti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnMultiplicar();
            }
        });

        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnDividir();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnLimpiar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnRegresar();
            }
        });
    }

    private void iniciarComponentes() {
        // Relacionar los obejtos
        btnSumar = (Button) findViewById(R.id.btnSumar);
        btnRestar = (Button) findViewById(R.id.btnRestar);
        btnMulti = (Button) findViewById(R.id.btnMultiplicar);
        btnDiv = (Button) findViewById(R.id.btnDividir);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);

        lblUsuario = (TextView) findViewById(R.id.lblUsuario);
        lblResultado = (TextView) findViewById(R.id.lblResultado);

        txtUno = (EditText) findViewById(R.id.txtNum1);
        txtDos = (EditText) findViewById(R.id.txtNum2);
    }
    private void btnSumar() {
        if(txtUno.getText().toString().isEmpty() || txtDos.getText().toString().isEmpty()){
            Toast.makeText(this, "Por favor, ingrese un número", Toast.LENGTH_SHORT).show();
        } else {
            calculadora.num1 = Float.parseFloat(txtUno.getText().toString());
            calculadora.num2 = Float.parseFloat(txtDos.getText().toString());
            float total = calculadora.suma();
            lblResultado.setText(String.valueOf(total));
        }
    }
    private void btnRestar() {
        if(txtUno.getText().toString().isEmpty() || txtDos.getText().toString().isEmpty()){
            Toast.makeText(this, "Por favor, ingrese un número", Toast.LENGTH_SHORT).show();
        } else {
            calculadora.num1 = Float.parseFloat(txtUno.getText().toString());
            calculadora.num2 = Float.parseFloat(txtDos.getText().toString());
            float total = calculadora.resta();
            lblResultado.setText(String.valueOf(total));
        }
    }
    private void btnMultiplicar() {
        if(txtUno.getText().toString().isEmpty() || txtDos.getText().toString().isEmpty()){
            Toast.makeText(this, "Por favor, ingrese un número", Toast.LENGTH_SHORT).show();
        } else {
            calculadora.num1 = Float.parseFloat(txtUno.getText().toString());
            calculadora.num2 = Float.parseFloat(txtDos.getText().toString());
            float total = calculadora.multiplicacion();
            lblResultado.setText(String.valueOf(total));
        }
    }
    private void btnDividir() {
        if(txtUno.getText().toString().isEmpty() || txtDos.getText().toString().isEmpty()){
            Toast.makeText(this, "Por favor, ingrese un número", Toast.LENGTH_SHORT).show();
        } else {
            calculadora.num1 = Float.parseFloat(txtUno.getText().toString());
            calculadora.num2 = Float.parseFloat(txtDos.getText().toString());
            float total = calculadora.division();
            lblResultado.setText(String.valueOf(total));
        }
    }
    private void btnLimpiar() {
        if(txtUno.getText().toString().isEmpty() && txtDos.getText().toString().isEmpty()){
            Toast.makeText(this, "Los campos ya están vacíos", Toast.LENGTH_SHORT).show();
        } else {
            lblResultado.setText("");
            txtUno.setText("");
            txtDos.setText("");
        }
    }
    private void btnRegresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("Regresar al MainActivity");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // No se realiza nada
            }
        });
        confirmar.show();
    }
}