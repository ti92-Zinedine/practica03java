package com.example.practica03java;

public class Calculadora {
    float num1;
    float num2;

    public Calculadora(float num1, float num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public float suma() {
        return num1 + num2;
    }

    public float resta() {
        return num1 - num2;
    }

    public float multiplicacion() {
        return num1 * num2;
    }

    public float division() {
        float total = 0;
        if (num2 != 0) {
            total = num1 / num2;
        }
        return total;
    }
}
